<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 15/01/19
 * Time: 10:54
 */

return [
    /*
    |----------------------------------------
    | Field Labels
    |----------------------------------------
    |
    */

    'labels' => [
        'singular' => 'Document',
        'plural' => 'Documents',
    ],
];