<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 14/01/19
 */

namespace YesWeDev\DocXChange;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Nova;
use Laravel\Nova\Events\ServingNova;

use YesWeDev\DocXChange\Nova\Documents;

class DocXChangeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/docxchange.php' => config_path('docxchange.php'),
        ], 'docxchange');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/docxchange.php',
            'docxchange'
        );

        $this->app->bind('document', function () {
            return new Document;
        });

        Nova::resources([
            Documents::class,
        ]);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['document'];
    }

}