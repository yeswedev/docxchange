<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 15/01/19
 * Time: 10:36
 */

namespace YesWeDev\DocXChange;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;
    protected $table = 'documents';

    protected $fillable = [
        'title',
        'doc_name',
        'description',
    ];
}