<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 16/01/19
 * Time: 10:58
 */

namespace YesWeDev\DocXChange\Policies;

use App\User;

use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

use YesWeDev\DocXChange\Traits\HasDocumentAuth;

class DocumentPolicy
{
    use HandlesAuthorization, HasDocumentAuth;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny($user)
    {
        return true;
    }

    public function view($user, $post)
    {
        return true;
    }

    public function create($user)
    {
        if($user->HasDocumentAuth('administrateur'))
        {
            return true;
        }
        return false;
    }

    public function update($user, $post)
    {
        if($user->hasRoleAdmin('administrateur'))
        {
            return true;
        }
    }

    public function delete($user, $post)
    {
        if($user->hasRoleAdmin('administrateur'))
        {
            return true;
        }
    }

    public function restore($user, $post)
    {
        return false;
    }

    public function forceDelete($user, $post)
    {
        return false;
    }
}
