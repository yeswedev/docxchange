<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 16/01/19
 * Time: 10:55
 */

namespace YesWeDev\DocXChange;

use Illuminate\Support\Facades\Gate;
use YesWeDev\DocXChange\Policies\DocumentPolicy;
use YesWeDev\LaravelModo\Policies\ModerationPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Document::class => DocumentPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
