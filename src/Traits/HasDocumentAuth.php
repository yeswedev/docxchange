<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 16/01/19
 * Time: 11:32
 */

namespace YesWeDev\DocXChange\Traits;

trait HasDocumentAuth
{
    public function HasDocumentAuth($role)
    {
        if(isset($this->roles) && ($this->roles->where('slug', $role)->first())){
            return true;
        }
        elseif(isset($this->role) && $this->role()->where('slug', $role)->first())
        {
            return true;
        }
        return false;

    }
}